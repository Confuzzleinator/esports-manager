
const routes = [
  {
    path: '/',
    component: () => import('layouts/TitleLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/new',
    component: () => import('layouts/TitleLayout.vue'),
    children: [
      { path: '', component: () => import('pages/New.vue') }
    ]
  },
  {
    path: '/standings',
    component: () => import('layouts/GameLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Standings.vue') }
    ]
  },
  {
    path: '/schedule',
    component: () => import('layouts/GameLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Schedule.vue') }
    ]
  },
  {
    path: '/roster/:team',
    component: () => import('layouts/GameLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Roster.vue') }
    ]
  },
  {
    path: '/freeagents',
    component: () => import('layouts/GameLayout.vue'),
    children: [
      { path: '', component: () => import('pages/FreeAgents.vue') }
    ]
  },
  {
    path: '/playoffs',
    component: () => import('layouts/GameLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Playoffs.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
