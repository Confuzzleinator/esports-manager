import Vue from 'vue'
import Vuex from 'vuex'
import save from './save'

Vue.use(Vuex)

const Store = new Vuex.Store({
  modules: {
    save
  }
})

export default Store
