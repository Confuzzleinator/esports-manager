export function advanceWeek(state) {
  state.currentWeek++
}
export function setTeam(state, id) {
  state.team = id
}
export function setSaveName(state, name) {
  state.name = name
}
export function setSchedule(state, schedule) {
  state.schedule = schedule
}
export function setGameWinner(state, {game, winner}) {
  state.schedule[state.currentWeek][game].winner = winner
}
export function teamWin(state, team) {
  state.teams[team].wins++
}
export function teamLoss(state, team) {
  state.teams[team].losses++
}
export function clearSchedule(state) {
  state.schedule = []
}
export function advanceSeason(state) {
  state.season++
}
export function resetWeek(state) {
  state.currentWeek = 0
}
export function resetWinLoss(state) {
  for(var i = 0; i < state.teams.length; i++) {
    state.teams[i].wins = 0
    state.teams[i].losses = 0
  }
}
export function setPlayerTeam(state, { player, team }) {
  var currentTeam = state.players[player].team

  // If the player is not a free agent or retired
  if(currentTeam >= 0) {
    // Removes player from roster of team
    for(var i = 0; i < state.teams[currentTeam].players.length; i++) {
      if(state.teams[currentTeam].players[i] == player) {
        state.teams[currentTeam].players.splice(i, 1)
        break
      }
    }
  }

  // Sets player's team to new team
  state.players[player].team = team
  // Adds player to roster of new team (if new team is not free agents or retired)
  if(team >= 0) {
    state.teams[team].players.push(player)
  }
}
export function claimTag(state, tag) {
  state.tags.available.splice(tag, 1) // Removes tag from available list
}
export function resetTags(state) {
  state.tags.available = state.tags.all.slice(0)
}
export function addPlayer(state, player) {
  state.players.push(player)
}
export function setPlayerContract(state, {player, amount, end}) {
  state.players[player].contract.amount = amount
  state.players[player].contract.end = end
}
export function agePlayer(state, player) {
  state.players[player].age += 1
}
export function modifyPlayerRating(state, {player, amount}) {
  state.players[player].rating += amount
  if(state.players[player].rating > 100) {
    state.players[player].rating = 100
  } else if(state.players[player].rating < 30) {
    state.players[player].rating = 30
  }
}
export function setLineup(state, {team, value}) {
  state.teams[team].players = value
}
export function setPhase(state, phase) {
  if(phase == 0) {
    state.phase.status = 'Preseason'
  } else if(phase == 1) {
    state.phase.status = 'Regular Season'
  } else if(phase == 2) {
    state.phase.status = 'Playoffs'
  }
  state.phase.id = phase
}
export function addOffer(state, {team, player, length, amount}) {
  state.players[player].offers.push({
    team: team,
    length: length,
    amount: amount
  })
}
export function clearOffers(state, player) {
  state.players[player].offers = []
}
export function setPlayerDevelopment(state, {player, amount}) {
  state.players[player].development = amount
}
export function addPlayoffSeries(state, {t1, t2}) {
  state.playoffs.series.push({
    t1: t1,
    t2: t2,
    t1Wins: 0,
    t2Wins: 0,
    winner: null
  })
}
export function setPlayoffWinner(state, {series, winner}) {
  if(state.playoffs.series[series].t1 == winner) {
    state.playoffs.series[series].t1Wins++
  } else {
    state.playoffs.series[series].t2Wins++
  }
}
export function clearPlayoffSchedule(state) {
  state.playoffs.series = []
}
export function setPlayoffTeams(state, teams) {
  state.playoffs.teams = teams
}
export function setPlayoffSeriesWinner(state, {series, team}) {
  state.playoffs.series[series].winner = team
}
export function advancePlayoffRound(state) {
  state.playoffs.round++
}
export function resetPlayoffRound(state) {
  state.playoffs.round = 0
}
export function setState(state, newState) {
  Object.assign(state, newState)
}
