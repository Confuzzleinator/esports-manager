export function resolveGame({commit}, { game, winner, loser }) {
  commit('setGameWinner', { game, winner })
  commit('teamWin', winner)
  commit('teamLoss', loser)
}
export function advanceSeason({commit}) {
  //commit('clearSchedule')
  //commit('clearPlayoffSchedule')
  commit('resetPlayoffRound')
  commit('advanceSeason')
  commit('resetWeek')
  //commit('resetWinLoss')
  commit('setPhase', 0)
}
