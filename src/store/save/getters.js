export function getTeamById(state) {
  return (id) => {
    for(var i = 0; i < state.teams.length; i++) {
      if(state.teams[i].id == id) {
        return state.teams[i]
      }
    }
    return null // If team cannot be found, return null
  }
}

export function getFreeAgents(state) {
  var freeAgents = []
  for(var i = 0; i < state.players.length; i++) {
    if(state.players[i].team == -1) {
      freeAgents.push(i)
    }
  }
  return freeAgents
}
