export default {
  name: '',
  team: null,
  currentWeek: 0,
  season: 2019,
  phase: {
    id: 0,
    text: 'Advance',
    status: 'Preseason'
  },
  schedule: null,
  playoffs: {
    round: 0,
    teams: [],
    series: []
  },
  teams: [
    {
      id: 0,
      name: 'Counter Logic Gaming',
      abbrev: 'CLG',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 0
    },
    {
      id: 1,
      name: 'Echo Fox',
      abbrev: 'FOX',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 1
    },
    {
      id: 2,
      name: 'Team Liquid',
      abbrev: 'TL',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 0
    },
    {
      id: 3,
      name: '100 Thieves',
      abbrev: '100',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 1
    },
    {
      id: 4,
      name: 'Cloud9',
      abbrev: 'C9',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 1
    },
    {
      id: 5,
      name: 'Clutch Gaming',
      abbrev: 'CG',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 1
    },
    {
      id: 6,
      name: 'FlyQuest',
      abbrev: 'FLY',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 1
    },
    {
      id: 7,
      name: 'Golden Guardians',
      abbrev: 'GG',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 1
    },
    {
      id: 8,
      name: 'Optic Gaming',
      abbrev: 'OPT',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 1
    },
    {
      id: 9,
      name: 'Team Solo Mid',
      abbrev: 'TSM',
      wins: 0,
      losses: 0,
      players: [],
      strategy: 1
    }
  ],
  players: [],
  countries: [
    {
      name: 'United States of America',
      abbrev: 'USA',
      region: 'NA',
      strength: 80,
      names: 0
    },
    {
      name: 'Canada',
      abbrev: 'CAN',
      region: 'NA',
      strength: 20,
      names: 0
    },
    {
      name: 'Germany',
      abbrev: 'GER',
      region: 'EU',
      strength: 40,
      names: 1
    },
    {
      name: 'Denmark',
      abbrev: 'DEN',
      region: 'EU',
      strength: 60,
      names: 2
    }
  ],
  names: {
    first: [
      [
        'John',
        'James',
        'Michael',
        'Peter',
        'Jordan',
        'Filbert'
      ],
      [
        'Abel',
        'Derek',
        'Bernard',
        'Johan',
        'Lucas',
        'Mathias'
      ],
      [
        'William',
        'Noah',
        'Lucas',
        'Alexander',
        'Emil',
        'Mikkel',
        'Thor'
      ]
    ],
    last: [
      [
        'Smith',
        'Johnson',
        'Brown',
        'Parker'
      ],
      [
        'Muller',
        'Schmidt',
        'Schneider',
        'Wagner'
      ],
      [
        'Jensen',
        'Nielsen',
        'Pedersen',
        'Christensen'
      ]
    ]
  },
  tags: {
    available: [],
    all: [
      'Piglet',
      'Imagine',
      'xkcd',
      'Dragon',
      'Drake',
      'Peanut',
      'Darshan',
      'Bjergsen',
      'Dardoch',
      'Confuzzle',
      'Kanzut',
      'Pokira',
      'Klorginshnackle',
      'Zakril',
      'Zaded',
      'Ponsoke',
      'Plesmaz',
      'Mewwly',
      'Verlis',
      'Liadnit',
      'Stellindrad',
      'HulkingDread',
      'Ignaceous',
      'Blighted Ruin',
      'Morose Newt',
      'Attilio',
      'Was_Abi',
      'Faceless',
      'Fells',
      'Balls',
      'Bang',
      'Faker',
      'GoldenGlue',
      'Nisqy',
      'Licorice',
      'Jensen',
      'Hex',
      'Mykix',
      'Rekless',
      'Caps',
      'Riley',
      'Rileymoogoo',
      'Snickerboy',
      'Huhi',
      'Huni',
      'Incarnation',
      'Assassin',
      'Freeze',
      'Auto',
      'Fill',
      'Wild',
      'Vander',
      'Forg1ven',
      'Wolf',
      'Maddix',
      'Fresco',
      'Party',
      'Animal',
      'Forge',
      'Doublelift',
      'Vapora',
      'Vapor',
      'Vanish',
      'Punisher',
      'Captain',
      'Plasma',
      'Plasmatic',
      'Phantic',
      'Phantom',
      'Haxxor',
      'Elite',
      'Helios',
      'Hun',
      'Cretin',
      'Likeamaws',
      'Velvet Angel',
      'Chaox',
      'Doublebread',
      'Riona',
      'Tyler1',
      'Tyler2',
      'imaqtpie',
      'Dyrus',
      'Bjergerking',
      'Tom Cruise',
      'BIG',
      'Big Summer',
      'Contractz',
      'Death Weasel',
      'Attilio the Hun',
      'WhiteKnight',
      'Solo',
      'Soligo',
      'Marko',
      'Fred',
      'Yusui',
      'Ryu',
      'Rango',
      'Rambo',
      'Quirk',
      'Linsanity',
      'Queasy',
      'Four',
      'Wild',
      'Warg',
      'Wickd',
      'Womprat',
      'Abelar',
      'Aris',
      'Bandit'
    ]
  }
}
