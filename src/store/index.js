import Vue from 'vue'
import Vuex from 'vuex'
import Store from './store.js'



/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  return Store
}
