import Store from '../store/store.js'
import { createDesiredContract } from './players.js'

function signPlayers(team) {
  // Don't mess with the player's team
  if(Store.state.save.team == team) {
    return
  }

  var open = openPositions(team)

  for(var i = 0; i < open.length; i++) {
    signPlayerForPos(team, open[i])
  }
}

function signPlayerForPos(team, pos) {
  var p = highestValuePlayer(pos, team)
  var c = calcOffer(team, p)

  Store.commit('save/addOffer', {team: team, player: p, amount: c.amount, length: c.length})
}

function bestPlayerInPos(pos, team) {
  var player = null;
  for(var i = 0; i < Store.state.save.players.length; i++) {
    if(Store.state.save.players[i].team == team && Store.state.save.players[i].pos == pos) {
      if(player == null) {
        player = i
      }
      if(Store.state.save.players[i].rating > Store.state.save.players[player].rating) {
        player = i
      }
    }
  }
  return player
}

function highestValuePlayer(pos, team) {
  var topPlayer = null
  var topVal = 0
  for(var i = 0; i < Store.state.save.players.length; i++) {
    if(Store.state.save.players[i].team == -1 && Store.state.save.players[i].pos == pos) {
      var value = Store.state.save.players[i].rating
      value += Math.floor(Math.random() * 7 - 3) // Between -3 and 3
      value -= 2 * Store.state.save.players[i].offers.length // Player is valued less the more offers they have
      if(Store.state.save.teams[team].strategy == 0) { // Rebuilding
        if(Store.state.save.players[i].age < 20) {
          value += 5
        } else if(Store.state.save.players[i].age < 22) {
          value += 2
        } else if(Store.state.save.players[i].age > 24) {
          value -= 5
        }
      }
    }

    if(value > topVal) {
      topVal = value
      topPlayer = i
    }
  }
  return topPlayer
}

function openPositions(team) {
  var openPos = []
  var hasPos = [false, false, false, false, false] // The order: TOP, JGL, MID, BOT, SUP
  for(var i = 0; i < Store.state.save.teams[team].players.length; i++) {
    var player = Store.state.save.teams[team].players[i]
    if(Store.state.save.players[player].pos == 'TOP') {
      hasPos[0] = true
    } else if(Store.state.save.players[player].pos == 'JGL') {
      hasPos[1] = true
    } else if(Store.state.save.players[player].pos == 'MID') {
      hasPos[2] = true
    } else if(Store.state.save.players[player].pos == 'BOT') {
      hasPos[3] = true
    } else if(Store.state.save.players[player].pos == 'SUP') {
      hasPos[4] = true
    }
  }

  if(!hasPos[0]) {
    openPos.push('TOP')
  }
  if(!hasPos[1]) {
    openPos.push('JGL')
  }
  if(!hasPos[2]) {
    openPos.push('MID')
  }
  if(!hasPos[3]) {
    openPos.push('BOT')
  }
  if(!hasPos[4]) {
    openPos.push('SUP')
  }

  return openPos
}

function sortTeamLineup(team) {
  // New lineup
  var tempLineup = []
  // Original lineup
  var original = Store.state.save.teams[team].players
  // Helps transition from original to new lineup
  var modified = original

  var top = bestPlayerInPos('TOP', team)
  var jungle = bestPlayerInPos('JGL', team)
  var mid = bestPlayerInPos('MID', team)
  var bot = bestPlayerInPos('BOT', team)
  var sup = bestPlayerInPos('SUP', team)

  // Add best players in each position to the top of the lineup
  if(top != null) {
    tempLineup.push(top)
    modified.splice(original.indexOf(top), 1)
  }
  if(jungle != null) {
    tempLineup.push(jungle)
    modified.splice(original.indexOf(jungle), 1)
  }
  if(mid != null) {
    tempLineup.push(mid)
    modified.splice(original.indexOf(mid), 1)
  }
  if(bot != null) {
    tempLineup.push(bot)
    modified.splice(original.indexOf(bot), 1)
  }
  if(sup != null) {
    tempLineup.push(sup)
    modified.splice(original.indexOf(sup), 1)
  }

  // Add all remaining players at the bottom of the new lineup
  for(var i = 0; i < modified.length; i++) {
    tempLineup.push(modified[i])
  }

  return tempLineup
}

function calcOffer(team, player) {
  var offer = {}
  var rand = Math.floor(Math.random() * 21 - 10) // Number between -10 and 10
  offer.amount = Store.state.save.players[player].rating + rand
  offer.length = Math.floor(Math.random() * 3 + 1) // Number between 1 and 3
  return offer
}

export { signPlayers, openPositions, sortTeamLineup }
