import Store from '../store/store.js'
import { Notify } from 'quasar'

// ----------------------------
// Generates (amount) of players
// ----------------------------
function genPlayers(amount, rookies) {
  var weighted = []
  // For each country, add (strength) copies of it to the weighted array
  for(var i = 0; i < Store.state.save.countries.length; i++) {
    for(var j = 0; j < Store.state.save.countries[i].strength; j++) {
      weighted.push(i)
    }
  }

  for(var i = 0; i < amount; i++) {
    var country = Math.floor(Math.random() * weighted.length)
    Store.commit('save/addPlayer', genPlayer(weighted[country], rookies))
  }
}

// ----------------------------------------
// Generates a single player and returns it
// ----------------------------------------
function genPlayer(country, rookie) {
  var player = {}// The object we are building
  // Creates a reference to the country object
  var c = Store.state.save.countries[country];

  // ID, TEAM, FIRST, LAST, CONTRACT, OFFERS, DEVELOPMENT
  player.id = Store.state.save.players.length
  player.team = -1 // Set newly generated players as free agents
  player.first = Store.state.save.names.first[c.names][Math.floor(Math.random() * Store.state.save.names.first[c.names].length)]
  player.last = Store.state.save.names.last[c.names][Math.floor(Math.random() * Store.state.save.names.last[c.names].length)]
  player.contract = {}
  player.offers = []
  player.development = 0

  // TAG
  // If we run out of tags, refresh tag list
  if(Store.state.save.tags.available.length <= 0) {
    Store.commit('save/resetTags')
  }
  var tag = Math.floor(Math.random() * Store.state.save.tags.available.length)
  player.tag = Store.state.save.tags.available[tag]
  Store.commit('save/claimTag', tag)

  // POSITION
  var pos = Math.floor(Math.random() * 5)
  switch(pos) {
    case 0:
      player.pos = 'TOP'
      break
    case 1:
      player.pos = 'JGL'
      break
    case 2:
      player.pos = 'MID'
      break
    case 3:
      player.pos = 'BOT'
      break
    case 4:
      player.pos = 'SUP'
  }

  // AGE, RATING
  if(rookie) {
    player.age = 18
    var rand = Math.random()
    if(rand < 0.5) { // 50%
      player.rating = Math.floor(Math.random() * 10 + 30)
    } else if(rand < 0.9) { // 40%
      player.rating = Math.floor(Math.random() * 10 + 40)
    } else { // 10%
      player.rating = Math.floor(Math.random() * 10 + 50)
    }
  } else {
    // If not a rookie
    var rand = Math.random()
    if(rand < 0.25) { // 25%
      player.age = Math.floor(Math.random() * 2 + 18)
    } else if(rand < 0.5) {
      player.age = Math.floor(Math.random() * 2 + 20)
    } else if(rand < 0.75) {
      player.age = Math.floor(Math.random() * 2 + 22)
    } else {
      player.age = Math.floor(Math.random() * 2 + 24)
    }

    rand = Math.random()
    if(rand < 0.5) { // 50%
      player.rating = Math.floor(Math.random() * 20 + 30)
    } else if(rand < 0.9) { // 40%
      player.rating = Math.floor(Math.random() * 20 + 50)
    } else { // 10%
      player.rating = Math.floor(Math.random() * 20 + 70)
    }
  }

  // RATING


  return player
}

// ----------------------------------------
// Creates a contract that the player wants
// ----------------------------------------
function createDesiredContract(player) {
  var contract = {}
  var player = Store.state.save.players[player]
  if(player.age <= 21) {
    contract.length = 3
  } else if(player.age <= 24) {
    contract.length = 2
  } else {
    contract.length = 1
  }

  contract.amount = player.rating

  return contract
}

// --------------------------------
// Increases all player's ages by 1
// --------------------------------
function agePlayers() {
  for(var i = 0; i < Store.state.save.players.length; i++) {
    Store.commit('save/agePlayer', i)
  }
}

// -------------------------
// Modifies player's ratings
// -------------------------
function developPlayers() {
  for(var i = 0; i < Store.state.save.players.length; i++) {
    if(Store.state.save.players[i].team != -2) { // Don't develop retired players
      if(checkPlayerRetirement(i)) {
        // If this player was on your team, let you know he retired
        if(Store.state.save.players[i].team == Store.state.save.team) {
          Notify.create({
            message: Store.state.save.players[i].tag + ' has retired',
            color: 'negative',
            timeout: 2000
          })
        Store.commit('save/setPlayerTeam', {player: i, team: -2})
        }
      } else {
        var development = calcPlayerDevelopment(i)
        Store.commit('save/modifyPlayerRating', {player: i, amount: development})
        Store.commit('save/setPlayerDevelopment', {player: i, amount: development})
      }
    }
  }
}

// --------------------------------------------------------
// Calculates how much a player should develop based on age
// --------------------------------------------------------
function calcPlayerDevelopment(player) {
  var type = Math.random()
  if(Store.state.save.players[player].age < 20) { // 18-19

    if(type < 0.05) { // 5% chance
      return Math.floor(Math.random() * 9 + 7) // Between 7 and 15
    } else if(type < 0.55) { // 50% chance
      return Math.floor(Math.random() * 4 + 3) // Between 3 and 6
    } else if(type < 0.85) { // 30% chance
      return Math.floor(Math.random() * 5 - 2) // Between -2 and 2
    } else if(type < 0.95) { // 10% chance
      return Math.floor(Math.random() * 4 - 6) // Betweeen -6 and -3
    } else { // 5% chance
      return Math.floor(Math.random() * 9 - 15) // Between -15 and -7
    }

  } else if(Store.state.save.players[player].age < 22) { // 20-21

    if(type < 0.02) { // 2% chance
      return Math.floor(Math.random() * 9 + 7) // Between 7 and 15
    } else if(type < 0.42) { // 40% chance
      return Math.floor(Math.random() * 4 + 3) // Between 3 and 6
    } else if(type < 0.82) { // 40% chance
      return Math.floor(Math.random() * 5 - 2) // Between -2 and 2
    } else if(type < 0.95) { // 13% chance
      return Math.floor(Math.random() * 4 - 6) // Betweeen -6 and -3
    } else { // 5% chance
      return Math.floor(Math.random() * 9 - 15) // Between -15 and -7
    }

  } else if(Store.state.save.players[player].age < 24) { // 22-23

    if(type < 0.01) { // 1% chance
      return Math.floor(Math.random() * 9 + 7) // Between 7 and 15
    } else if(type < 0.26) { // 25% chance
      return Math.floor(Math.random() * 4 + 3) // Between 3 and 6
    } else if(type < 0.76) { // 50% chance
      return Math.floor(Math.random() * 5 - 2) // Between -2 and 2
    } else if(type < 0.95) { // 19% chance
      return Math.floor(Math.random() * 4 - 6) // Betweeen -6 and -3
    } else { // 5% chance
      return Math.floor(Math.random() * 9 - 15) // Between -15 and -7
    }

  } else if(Store.state.save.players[player].age < 26) { // 24-25

    if(type < 0.2) { // 20% chance
      return Math.floor(Math.random() * 4 + 3) // Between 3 and 6
    } else if(type < 0.7) { // 50% chance
      return Math.floor(Math.random() * 5 - 2) // Between -2 and 2
    } else if(type < 0.95) { // 25% chance
      return Math.floor(Math.random() * 4 - 6) // Betweeen -6 and -3
    } else { // 5% chance
      return Math.floor(Math.random() * 9 - 15) // Between -15 and -7
    }

  } else { // 26+

    if(type < 0.1) { // 10% chance
      return Math.floor(Math.random() * 4 + 3) // Between 3 and 6
    } else if(type < 0.6) { // 50% chance
      return Math.floor(Math.random() * 5 - 2) // Between -2 and 2
    } else if(type < 0.9) { // 30% chance
      return Math.floor(Math.random() * 4 - 6) // Betweeen -6 and -3
    } else { // 10% chance
      return Math.floor(Math.random() * 9 - 15) // Between -15 and -7
    }

  }
}

// Determines whether player will retire
// Players over 23 have a 50% chance to
// retire if they are not on a team and
// players over 25 on a team have a 20%
// chance to retire
function checkPlayerRetirement(player) {
  if(Store.state.save.players[player].age >= 21) {
    if(Store.state.save.players[player].team != -1) {
      if(Store.state.save.players[player].age >= 24) {
        var rand = Math.random()
        if(rand < 0.2) {
          return true
        }
      }
    } else {
      var rand = Math.random()
      if(rand < 0.5) {
        return true
      }
    }
  }
  return false
}

// Calculates how highly the player considers a certain
// contract offer. Players tend to sign with the team
// that offers them the highest value contract
function calcOfferValue(offer) {
  var value = 0
  value += offer.amount
  if (offer.length == 2) {
    value += 5
  } else if(offer.length == 3) {
    value += 10
  }

  return value
}

// The player calculates the value of each offer they have
// and signs with the team that offered the highest value
// contract
function chooseOffer(player) {
  // Don't mess with players that didn't get any offers
  if(Store.state.save.players[player].offers.length < 1) {
    return
  }

  var playerOffer = false
  var contract = null
  var topVal = 0
  for(var i = 0; i < Store.state.save.players[player].offers.length; i++) {
    var val = calcOfferValue(Store.state.save.players[player].offers[i])
    var fullRoster = false
    // If a team already has 10 players, the player cannot sign with them
    if(Store.state.save.teams[Store.state.save.players[player].offers[i].team].players.length >= 10) {
      fullRoster = true
      if(Store.state.save.team == Store.state.save.players[player].offers[i].team) {
        Notify.create({
          message: Store.state.save.players[player].tag + ' cannot sign with you as your roster is full',
          color: 'negative',
          timout: 2000
        })
      }
    }
    if(val > topVal && !fullRoster) {
      topVal = val
      contract = i
    }

    if(Store.state.save.players[player].offers[i].team == Store.state.save.team) {
      playerOffer = true
    }
  }

  // If the player has made an offer for this player, let them know of the
  // player's decision
  if(playerOffer && contract != null) {
    if(Store.state.save.players[player].offers[contract].team == Store.state.save.team) {
      Notify.create({
        message: Store.state.save.players[player].tag + ' has chosen to accept your offer!',
        color: 'positive',
        timeout: 2000
      })
    } else {
      Notify.create({
        message: Store.state.save.players[player].tag + ' has chosen to sign with ' + Store.state.save.teams[Store.state.save.players[player].offers[contract].team].abbrev,
        color: 'negative',
        timeout: 2000
      })
    }
  }

  // If the player was able to find an acceptable team
  if(contract != null) {
    Store.commit('save/setPlayerContract', {player: player, amount: Store.state.save.players[player].offers[contract].amount, end: Store.state.save.season + Store.state.save.players[player].offers[contract].length})
    Store.commit('save/setPlayerTeam', {player: player, team: Store.state.save.players[player].offers[contract].team})
  }

  Store.commit('save/clearOffers', player)
}

export { genPlayers, createDesiredContract, agePlayers, developPlayers, chooseOffer }
