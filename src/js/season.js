import Store from '../store/store.js'
import { genSchedule } from './schedule.js'
import { genPlayers, agePlayers, developPlayers } from './players.js'
import { signPlayers } from './ai.js'

function resolveContract(player) {
  // Don't mess with players not on a team
  if(Store.state.save.players[player].team < 0) {
    return
  }

  if(Store.state.save.players[player].contract.end <= Store.state.save.season) { // If player's contract expires
    Store.commit('save/setPlayerTeam', { player: player, team: -1 }) // Set player's team to -1 which represents a free agent
  }
}

function resolveContracts() {
  for(var i = 0; i < Store.state.save.players.length; i++) {
    if(Store.state.save.players[i].team != -1) { // Ignore existing free agents
      resolveContract(i)
    }
  }
}

function endSeason() {
  developPlayers()
  agePlayers()
  resolveContracts()
  var teamIds = []
  genPlayers(50, true) // Generate next season's rookies
  // AI teams create their first round of offers
  for(var i = 0; i < Store.state.save.teams.length; i++) {
    if(Store.state.save.teams[i].id != Store.state.save.team) { // Don't mess with the player's team
      signPlayers(i)
    }
  }
  Store.dispatch('save/advanceSeason')
}

export { resolveContracts, endSeason }
