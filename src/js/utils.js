import Store from '../store/store.js'

export function getStandings() {
  var standings = []
  var temp = []
  for(var i = 0; i < Store.state.save.teams.length; i++) {
    temp.push(Store.state.save.teams[i])
  }
  // While there are teams left to be sorted
  while(temp.length > 0) {
    var topWins = -1
    var location = null
    var topTeam = null
    // Loops through every team left to be sorted and sets topTeam
    // to the team with the highest wins of those left
    for(var i = 0; i < temp.length; i++) {
      if(temp[i].wins > topWins) {
        topWins = temp[i].wins
        topTeam = temp[i].id
        location = i
      }
    }

    // Remove the top team from those left to be sorted and add it to
    // the standings
    temp.splice(location, 1)
    standings.push(topTeam)
  }

  return standings
}

export function rand(min, max) {
  return Math.floor(Math.random()*(max-min+1)+min);
}

export function randSelect(weights) {
  var total = 0
  for(var i = 0; i < weights.length; i++) {
    total += weights[i]
  }

  var checked = 0
  var r = rand(1, total)
  for(var i = 0; i < weights.length; i++) {
    if(r <= weights[i] + checked) {
      return i
    }
    checked += weights[i]
  }
}
