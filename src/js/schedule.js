import Store from '../store/store.js'

function genSchedule(teams, rounds) {
  // Generates round robin schedule
  if(teams.length % 2 == 1) { // If there are an odd number of teams, add in a bye
    teams.push(-1) // All normal teams have an id greater than zero, so -1 represents a bye
  }

  var size = teams.length / 2 // Find median of teams
  var sides = []
  var schedule = []
  sides[0] = teams.slice(0, size) // First half to the first side
  sides[1] = teams.slice(size, teams.length) // Second half to the other side

  // Pushing each week to the schedule instead of rounds as a whole makes it easier
  // to display the schedule using v-for without creating seperate divisions for
  // rounds but treating it as one continuous schedule made up of weeks
  for(var i = 0; i < rounds; i++) { // For each round
    var round = scheduleRound(sides[0], sides[1]) // Schedule the round and store it in an array
    for(var j = 0; j < round.length; j++) { // For each week in the round
      schedule.push(round[j]) // Add it to the schedule
    }
  }
  // Set schedule in store
  Store.commit('save/setSchedule', schedule)
}

function scheduleRound(side1, side2) {
  var sides = [side1, side2]
  var teams = side1.length + side2.length
  var round = []
  for(var i = 0; i < teams - 1; i++) { // For each week of the round
    round.push(scheduleWeek(sides[0], sides[1]))
    sides = shift(sides[0], sides[1])
  }
  return round
}

function scheduleWeek(side1, side2) {
  var teams = side1.length + side2.length // Find total number of teams participating
  var week = []
  for(var i = 0; i < teams / 2; i++) { // For every game that needs to be played
    week.push({
      t1: side1[i],
      t2: side2[i],
      winner: null
    }) // Schedule game between teams at the same index on opposite sides
  }
  return week
}

function shift(side1, side2) {
  // Store the new sides after shifting
  var new1 = []
  var new2 = []

  new1[0] = side1[0] // Freeze the first element
  // Shift values while keeping one frozen (by skipping the first element) for side 1
  for(var i = 1; i < side1.length; i++) {
    if(i == side1.length -1) { // If we are on the last element of the first side
      new2[side2.length - 1] = side1[i] // Put the element at the end of the second side
    } else {
      new1[i + 1] = side1[i] // If not the last element, shift the element over one
    }
  }

  // Shift values for side 2
  for(var i = 0; i < side2.length; i++) {
    if(i == 0) { // If we are on the first element of the second side
      new1[1] = side2[i] // Put the element in the second slot of the first slide (because the first slot is frozen)
    } else {
      new2[i - 1] = side2[i] // If not the last element, shift the element back one
    }
  }

  return [new1, new2]
}

export { genSchedule }
