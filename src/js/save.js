import fs from 'fs'
import path from 'path'
import Store from '../store/store.js'
import { Events } from './events.js'
import { resetState } from './utils.js'
import { Notify } from 'quasar'

const app = require('electron').remote.app

function save(name) {
  fs.writeFile(app.getPath('userData') + path.sep + 'saves' + path.sep + name + '.json', JSON.stringify(Store.state.save, null, 2), (err, data) => {
    if(err) {
      console.log(err)
      Notify.create({
        message: 'Could not save, yell at Confuzzle',
        color: 'negative',
        timout: 1000
      })
    } else {
      Notify.create({
        message: 'Saved!',
        color: 'positive',
        timeout: 1000
      })
    }
  })
}

function load(name) {
  fs.readFile(app.getPath('userData') + path.sep + 'saves' + path.sep + name, (err, data) => {
    if(err) {
      console.log(err)
    }
    Store.commit('save/setState', JSON.parse(data))
    Events.$emit('advance')
  })
}

function deleteSave(name) {
  fs.unlink(app.getPath('userData') + path.sep + 'saves' + path.sep + name, (err) => {
    if(err) {
      console.log(err)
    }
    Events.$emit('deleted')
  })
}

export { save, load, deleteSave }
