import Store from '../store/store.js'
import { Events } from './events.js'
import { genSchedule } from './schedule.js'
import { endSeason } from './season.js'
import { signPlayers, openPositions, sortTeamLineup } from './ai.js'
import { chooseOffer } from './players.js'
import { Notify } from 'quasar'
import { getStandings, rand } from './utils.js'

function Game(t1, t2) {

  var rand = Math.random()

  if(rand < (1 / (1 + Math.pow(10, ((calcTeamRating(t2) - calcTeamRating(t1)) / 100))))) {
    return { winner: t1, loser: t2 }
  } else {
    return { winner: t2, loser: t1 }
  }
}

Game.prototype.calcAdvantage = function(t1, t2) {
  var t1Strength = 0
  var t2Strength = 0
  for(var i = 0; i < t1.length; i++) {
    if(t1[i].damaged) {
      t1Strength += t1[i].rating * damagedPenalty
    } else {
      t1Strength += t1[i].rating
    }
  }
  for(var i = 0; i < t2.length; i++) {
    if(t2[i].damaged) {
      t2Strength += t2[i].rating * damagedPenalty
    } else {
      t2Strength += t2[i].rating
    }
  }

  return (t1Strength - t2Strength) * ratingsMultiplier
}

// Set lineups for both teams and creates the GamePlayers
Game.prototype.setLineups = function() {
  for(var i = 0; i < 5; i++) {
    var pos = null
    switch(i) {
      case 0:
        pos = 'TOP'
        break
      case 1:
        pos = 'JGL'
        break
      case 2:
        pos = 'MID'
        break
      case 3:
        pos = 'BOT'
        break
      case 4:
        pos = 'SUP'
    }
    this.lineups[0].push(new GamePlayer(Store.state.save.teams[this.t1].players[i], pos))
  }
  for(var i = 0; i < 5; i++) {
    var pos = null
    switch(i) {
      case 0:
        pos = 'TOP'
        break
      case 1:
        pos = 'JGL'
        break
      case 2:
        pos = 'MID'
        break
      case 3:
        pos = 'BOT'
        break
      case 4:
        pos = 'SUP'
    }
    this.lineups[1].push(new GamePlayer(Store.state.save.teams[this.t2].players[i], pos))
  }
}

function GamePlayer(id, pos) {
  this.id = id
  this.pos = pos
  this.gold = 0
  this.tag = Store.state.save.players[id].tag
  this.rating = Store.state.save.players[id].rating
  this.kills = 0
  this.deaths = 0
  this.assists = 0
  this.damaged = false

  // Out of position penalties
  if(Store.state.save.players[this.id].pos != pos) {
    this.rating *= positionPenalty
  }
}

// Returns the total of the team's starter's ratings
function calcTeamRating(team) {
  var rating = 0
  for(var i = 0; i < 5; i++) { // Only count the first 5 players (starters)
    var pos = Store.state.save.players[Store.state.save.teams[team].players[i]].pos
    // If the player is in the proper position, they get the full benefit of their rating,
    // otherwise they suffer a 40% penalty
    if(pos == 'TOP' && i == 0) {
      rating += Store.state.save.players[Store.state.save.teams[team].players[i]].rating
    } else if(pos == 'JGL' && i == 1) {
      rating += Store.state.save.players[Store.state.save.teams[team].players[i]].rating
    } else if(pos == 'MID' && i == 2) {
      rating += Store.state.save.players[Store.state.save.teams[team].players[i]].rating
    } else if(pos == 'BOT' && i == 3) {
      rating += Store.state.save.players[Store.state.save.teams[team].players[i]].rating
    } else if(pos == 'SUP' && i == 4) {
      rating += Store.state.save.players[Store.state.save.teams[team].players[i]].rating
    } else {
      rating += 0.6 * Store.state.save.players[Store.state.save.teams[team].players[i]].rating
    }
  }

  return rating
}

export { Game }
