import Store from '../store/store.js'
import { Events } from './events.js'
import { genSchedule } from './schedule.js'
import { endSeason } from './season.js'
import { signPlayers, openPositions, sortTeamLineup } from './ai.js'
import { chooseOffer } from './players.js'
import { Notify } from 'quasar'
import { getStandings } from './utils.js'
import { Game } from './sim.js'

function advance() {
  // Resolve all offers free agents have gotten
  for(var i = 0; i < Store.state.save.players.length; i++) {
    if(Store.state.save.players[i].team == -1) {
      chooseOffer(i)
    }
  }
  // ---------
  // PRESEASON
  // ---------
  if(Store.state.save.phase.id == 0) {
    advancePreseason()
  }

  // --------------
  // REGULAR SEASON
  // --------------
  if(Store.state.save.phase.id == 1) {
    var a = advanceRegularSeason()
    if(a == 1) {
      return 1
    }
  }


  // --------
  // PLAYOFFS
  // --------
  if(Store.state.save.phase.id == 2) {
    var a = advancePlayoffs()
    if(a == 1) {
      return 0
    }
  }


  Store.commit('save/advanceWeek')
  Events.$emit('advance')

  // Move on to next phase
  if(Store.state.save.phase.id == 0) {
    if(Store.state.save.currentWeek >= 10) {
      // Enter regular season
      Store.commit('save/setPhase', 1)
      Store.commit('save/resetWeek')
      for(var i = 0; i < Store.state.save.teams.length; i++) {
        if(i != Store.state.save.team) {
          Store.commit('save/setLineup', { team: i, value: sortTeamLineup(i) })
        }
      }

      // Clear last season's data
      Store.commit('save/clearSchedule')
      Store.commit('save/clearPlayoffSchedule')
      Store.commit('save/resetWinLoss')

      // Generate the schedule
      var teamIds = []
      for(var i = 0; i < Store.state.save.teams.length; i++) {
        teamIds.push(Store.state.save.teams[i].id) // Put all teams from save in an array (just the id's)
      }
      genSchedule(teamIds, 2)

      Events.$emit('advance')
      return 0
    }
  } else if(Store.state.save.phase.id == 1) {
    if(Store.state.save.currentWeek >= Store.state.save.schedule.length) {
      setupPlayoffs()
      Store.commit('save/setPhase', 2)
      Store.commit('save/resetWeek')
    }
  }
}

function advancePreseason() {
  // Each week of preseason, AI teams can offer for players
  for(var i = 0; i < Store.state.save.teams.length; i++) {
    if(Store.state.save.teams[i].id != Store.state.save.team) { // Don't mess with the player's team
      signPlayers(i)
    }
  }
}

function advanceRegularSeason() {
  // Require the player to have at least 5 players
  if(Store.state.save.teams[Store.state.save.team].players.length < 5) {
    return 1
  }

  // Normal operations each week (simming any scheduled games)
  var gamesToSim = Store.state.save.schedule[Store.state.save.currentWeek] // Get all games scheduled for the week
  for(var i = 0; i < gamesToSim.length; i++) {
    if(gamesToSim[i].t1 != -1 && gamesToSim[i].t2 != -1) { // Make sure game is not a bye
      var game = new Game(gamesToSim[i].t1, gamesToSim[i].t2)
      Store.dispatch('save/resolveGame', { game: i, winner: game.winner, loser: game.loser })
      if(game.winner == Store.state.save.team || game.loser == Store.state.save.team) {
        if(game.winner == Store.state.save.team) {
          Notify.create({
            message: 'You team defeated ' + Store.state.save.teams[game.loser].abbrev,
            color: 'positive',
            timeout: 1000
          })
        } else {
          Notify.create({
            message: 'Your team lost to ' + Store.state.save.teams[game.winner].abbrev,
            color: 'negative',
            timeout: 1000
          })
        }
      }
    }
  }
}

function advancePlayoffs() {
  for(var i = 0; i < Store.state.save.playoffs.series.length; i++) {
    // If the series has not already finished simulate remaining games
    if(Store.state.save.playoffs.series[i].winner == null) {
      var game = Game(Store.state.save.playoffs.series[i].t1, Store.state.save.playoffs.series[i].t2)
      Store.commit('save/setPlayoffWinner', {series: i, winner: game.winner})

      // If both series are finished, move on to the next round
      if(Store.state.save.playoffs.series[i].t1Wins > 2) {
        Store.commit('save/setPlayoffSeriesWinner', {series: i, team: Store.state.save.playoffs.series[i].t1})
      } else if(Store.state.save.playoffs.series[i].t2Wins > 2) {
        Store.commit('save/setPlayoffSeriesWinner', {series: i, team: Store.state.save.playoffs.series[i].t2})
      }
    }
  }

  var seriesComplete = false
  for(var i = 0; i < Store.state.save.playoffs.series.length; i++) {
    if(Store.state.save.playoffs.series[i].winner != null) {
        seriesComplete = true
    } else {
      seriesComplete = false
      break
    }
  }
  // If we are ready to move on to the next round
  if(seriesComplete) {
    if(Store.state.save.playoffs.round == 0) { // If we just finished the first round
      // 1st seed vs 4v5 winner
      Store.commit('save/addPlayoffSeries', {t1: Store.state.save.playoffs.teams[0], t2: Store.state.save.playoffs.series[0].winner})
      // 2nd seed vs 3v6 winner
      Store.commit('save/addPlayoffSeries', {t1: Store.state.save.playoffs.teams[1], t2: Store.state.save.playoffs.series[1].winner})
      Store.commit('save/advancePlayoffRound')
    } else if(Store.state.save.playoffs.round == 1) { // If we just finished the semifinal
      // Winners of the semifinal matches
      Store.commit('save/addPlayoffSeries', {t1: Store.state.save.playoffs.series[2].winner, t2: Store.state.save.playoffs.series[3].winner})
      Store.commit('save/advancePlayoffRound')
    } else if(Store.state.save.playoffs.round == 2) {
      // Notify the player of the champion and advance to the next season
      Notify.create({
        message: Store.state.save.teams[Store.state.save.playoffs.series[4].winner].name + ' is your champion!',
        type: 'positive',
        position: 'center',
        timeout: 3000
      })

      // End the season after final
      endSeason()
      Events.$emit('advance')
      return 1
    }
  }
}


function setupPlayoffs() {
  var playoffTeams = []
  var standings = getStandings()
  for(var i = 0; i < 6; i++) {
    playoffTeams.push(standings[i])
  }
  Store.commit('save/setPlayoffTeams', playoffTeams)
  Store.commit('save/addPlayoffSeries', {t1: Store.state.save.playoffs.teams[3], t2: Store.state.save.playoffs.teams[4]}) // 4th and 5th seeds
  Store.commit('save/addPlayoffSeries', {t1: Store.state.save.playoffs.teams[2], t2: Store.state.save.playoffs.teams[5]}) // 3rd and 6th seeds
}

export { advance }
