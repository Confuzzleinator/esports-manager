# Esports Manager  
_Esports Manager is created and maintained by Javen Kazebee (Confuzzleinator)_  
## Setup
1. To set up, navigate to the cloned folder run the commands `npm init`
2. You are now ready to take on the world!
3. (You can test the project using `quasar dev -m electron`)
